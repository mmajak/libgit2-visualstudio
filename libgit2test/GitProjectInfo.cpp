//
// Created by mmajak on 20.2.2017.
//

#include "GitProjectInfo.h"
#include <iostream>
#include <map>
#include <algorithm>
#include <cstring>
#include <string>
#include <C:/Users/mmajak/Documents/Visual studio 2013/Projects/libgit2-0.25.1/include/git2.h>
/*#include <git2/sys/filter.h>
#include <git2/sys/repository.h>
#include <git2/sys/commit.h>
#include <git2/sys/diff.h>*/
//#include "git2/cred_helpers.h"
#include "C:/Users/mmajak/Documents/Visual studio 2013/Projects/libgit2-0.25.1/examples/common.h"
#include <fstream>
#include <sstream>
#include <conio.h>
#include <windows.h>
#include <io.h>
#include <C:/Users/mmajak/dirent.h/include/dirent.h>
#include <cstring>
#include <stdio.h>  
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <thread>
#include <vector>
#include <fstream>
#include <sys/stat.h>

#ifdef WINDOWS
#include <direct.h>
#define GetCurrentDir _getcwd
#else

//#include <git2/remote.h>

#define GetCurrentDir getcwd
#endif

GitProjectInfo::GitProjectInfo() {
	git_libgit2_init();
}

GitProjectInfo::~GitProjectInfo() {
	git_libgit2_shutdown();
}

// calling findNumOfRepo(), findSubfolders() and fillAllData() to fill package_info_map
void GitProjectInfo::getPackages(string path) {
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	counter = 0;
	all_paths.clear();
	package_info_map = map <string, PackageInfo>();

	findNumOfRepo(path);
	if (counter == 0) {
		SetConsoleTextAttribute(hConsole, 7);
		cerr << path << " do not include repositories" << endl;
		SetConsoleTextAttribute(hConsole, 7);
	}
	PackageInfo* package_info = new PackageInfo[counter];
	findSubfolders(path, package_info);
	fillAllData(package_info);
}

// calling fillData() for all .git repositories, using threads
void GitProjectInfo::fillAllData(PackageInfo* package_info) {
	package_info_map = map <string, PackageInfo>();
	vector<thread> threads;
	for (int i = 0; i < counter; ++i)	threads.push_back(thread(&GitProjectInfo::fillData, this, package_info, i));
	for (auto& th : threads)				th.join();
}

// calling fillFetchData() for all local paths, using threads
void GitProjectInfo::fillAllFetchData(vector<string> to_fetch_paths){

	CommitInfo* info_fetch = new CommitInfo[to_fetch_paths.size()];
	vector<thread> threads;
	for (unsigned int i = 0; i < to_fetch_paths.size(); ++i)	{
		threads.push_back(thread(&GitProjectInfo::fillFetchData, this, info_fetch, i, to_fetch_paths.at(i)));
	}
	for (auto& th : threads)				th.join();
}



// fill information about actuality of map<string,PackageInfo> package_info_map to package_info_map
void GitProjectInfo::fillFetchData(CommitInfo* info_fetch, int index, string path) {
	info_fetch[index] = checkIfActual(path);
	map <string, PackageInfo>::iterator it = package_info_map.find(path);
	if (it != package_info_map.end()) {
		it->second.commit_info = info_fetch[index].message;
		it->second.commit_number = info_fetch[index].number;
	}
}

// prints out contents of map <string, PackageInfo> package_info_map
void GitProjectInfo::print() {
	cout << endl;
	for (auto& kv : package_info_map) {
		cout << kv.second.local_path << "\t\t" << kv.second.url << "\t" << kv.second.commit_info << endl;
	}
}

// fill one item of map<string,PackageInfo> package_info_map
void GitProjectInfo::fillData(PackageInfo* package_info, int index) {
	package_info[index].local_path = findLocalPath(all_paths.at(index));
	package_info[index].url = findUrl(all_paths.at(index));
	string result = gitDescribe(all_paths.at(index));

	if (result.find("refs/heads/") != string::npos) result.erase(0, 11);
	else if (result.find("refs/tags/") != string::npos) result.erase(0, 10);
	else if (result.find("tags/") != string::npos) result.erase(0, 5);
	else if (result.find("heads/") != string::npos) result.erase(0, 6);
	package_info[index].tag = result;
	package_info_map.insert(pair<string, PackageInfo>(package_info[index].local_path, package_info[index]));
}

// walking through all subfolders and searching .git, filling vector<string> all_paths
void GitProjectInfo::findIfIncludeGit(string name, string path, PackageInfo* package_info) {

	size_t pos = name.find(".git");
	size_t pos1 = name.find("gitignore");
	if ((pos != string::npos) && (pos1 == string::npos)) {													// folder we search for

		const char * REPO_PATH = path.c_str();
		git_repository * repo = nullptr;
		if (git_repository_open(&repo, REPO_PATH) == 0) all_paths.push_back(path);

		git_repository_free(repo);
	}
	else if ((name != ".") && (name != "..")){									// searching continues, trying to find .git
		string add_path = path;
		add_path += "\\";
		add_path += name;
		findSubfolders(add_path, package_info);
	}
}

// open location folder, calling findIfIncludeGit()
void GitProjectInfo::findSubfolders(string path, PackageInfo* package_info) {
	DIR *dir;
	struct dirent *ent;
	const char* loc = path.c_str();
	if ((dir = opendir(loc)) != NULL) {
		while ((ent = readdir(dir)) != NULL) {                      // searching through all folders
			findIfIncludeGit(ent->d_name, path, package_info);
		}
		closedir(dir);
	}
}

// returns num of .git repositories in given path
void GitProjectInfo::findNumOfRepo(string path) {
	DIR *dir;
	struct dirent *ent;
	const char* loc = path.c_str();
	if ((dir = opendir(loc)) != NULL) {
		while ((ent = readdir(dir)) != NULL) {
			string name = ent->d_name;
			size_t pos = name.find(".git");
			size_t pos1 = name.find("gitignore");
			if ((pos != string::npos) && (pos1 == string::npos)) {												// folder we search for
				const char * REPO_PATH = path.c_str();
				git_repository * repo = nullptr;
				if (git_repository_open(&repo, REPO_PATH) == 0) counter++;
				git_repository_free(repo);
			}
			else if ((name != ".") && (name != "..")){                              // searching continues, trying to find .git
				string add_path = loc;
				add_path += "\\";
				add_path += name;
				findNumOfRepo(add_path);
			}
		}
		closedir(dir);
	}
}

// returns URL of repository from a given path
string GitProjectInfo::findUrl(string path) {
	const char * REPO_PATH = path.c_str();
	git_repository * repo = nullptr;
	git_repository_open(&repo, REPO_PATH);
	git_remote *remote = nullptr;
	int error = git_remote_lookup(&remote, repo, "origin");
	const char *url = nullptr;

	if (error < 0)  url = "Your repository is not remote\t";
	else{
		url = git_remote_url(remote);
	}
	string result = url;
	git_repository_free(repo);
	return result;
}

// returns local path of .git repository
string GitProjectInfo::findLocalPath(string path) {

	const char * REPO_PATH = path.c_str();
	git_repository * repo = nullptr;
	git_repository_open(&repo, REPO_PATH);

	const char *local;
	local = git_repository_path(repo);
	string result = local;
	git_repository_free(repo);
	result.erase(result.end() - 5, result.end());          // delete .git/ ending
	return result;
}

string name;
string password;
void GitProjectInfo::setCredentials() {
	string nam;
	cout << endl << "Enter username : ";
	cin >> nam;
	string pass = "";
	char ch;
	cout << "Enter password : ";
	ch = _getch();
	// character 13 is enter
	while (ch != 13){
		// backspace
		if ((pass.size() > 0) && (ch == 8)) {

			pass.clear();
			cout << "\r                                                                          ";
			cout << "\rEnter password : ";
			ch = _getch();
		}
		else if (ch == 8){ ch = _getch(); }
		else {
			pass.push_back(ch);
			cout << '*';
			ch = _getch();
		}
	}
	cout << endl;
	user_name = name = nam;
	user_pass = password = pass;
}

int cred_acquire_cb(git_cred **cred, const char *url, const char *username_from_url, unsigned int allowed_types, void *payload)
{
	git_cred_default_new(cred);
	if ((name.empty() == false) && (password.empty() == false)) git_cred_userpass_plaintext_new(cred, name.c_str(), password.c_str());
	return 0;
}

// returns number of remote commits
int GitProjectInfo::numOfRemoteCommits(git_repository *repo, string path) {
	int remote_commits = 0;
	string branch, branch_local, branch_remote;

	git_revwalk *walker;
	git_revwalk_new(&walker, repo);

	branch = getCurrentBranch(path);
	branch_local = "refs/heads/" + branch;
	branch_remote = "refs/remotes/origin/" + branch;

	git_revwalk_push_ref(walker, branch_remote.c_str());
	git_revwalk_hide_ref(walker, branch_local.c_str()); // refs/heads/master

	git_oid id;
	while (!git_revwalk_next(&id, walker))
	{
		++remote_commits;
	}

	git_revwalk_free(walker);
	return remote_commits;
}

// returns number of local commits
int GitProjectInfo::numOfLocalCommits(git_repository *repo, string path) {
	int local_commits = 0;
	string branch, branch_local, branch_remote;

	git_revwalk *walker;
	git_revwalk_new(&walker, repo);

	branch = getCurrentBranch(path);
	branch_local = "refs/heads/" + branch;
	branch_remote = "refs/remotes/origin/" + branch;

	git_revwalk_push_ref(walker, branch_local.c_str()); // refs/heads/master
	git_revwalk_hide_ref(walker, branch_remote.c_str());
	if (branch_local != "refs/heads/master") git_revwalk_hide_ref(walker, "refs/heads/master");

	git_oid id;
	while (!git_revwalk_next(&id, walker))
	{
		++local_commits;
	}

	git_revwalk_free(walker);
	return local_commits;
}

// returns information if program is actual or not thanks to num of local and remote commits, called by checkIfActual()
string GitProjectInfo::getInfoIfActual(int local_commits, int remote_commits, string path){
	string result;
	if ((remote_commits == 0) && (local_commits == 0)) result = "You have an actual version";
	else if (remote_commits > local_commits){
		ostringstream oss;
		oss << "Your branch is behind 'origin/" << getCurrentBranch(path) << "' by " << remote_commits << " commits";
		result = oss.str();
	}
	else if (remote_commits < local_commits) {
		ostringstream oss;
		oss << "Your branch is ahead of 'origin/" << getCurrentBranch(path) << "' by " << local_commits << " commits";
		result = oss.str();
	}
	else {
		ostringstream oss;
		oss << "Your branch is behind 'origin/" << getCurrentBranch(path) << "' by " << local_commits << " commits";
		result = oss.str();
	}
	return result;
}

// check if I have an actual version of program (using git_remote_fetch), returns information about repo
CommitInfo GitProjectInfo::checkIfActual(string path) {
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	string result;

	const char *REPO_PATH = path.c_str();
	int remote_commits = 0;
	int local_commits = 0;

	git_repository *repo = nullptr;
	if (git_repository_open(&repo, REPO_PATH) != 0)  {
		SetConsoleTextAttribute(hConsole, 12);
		cerr << "Problem occured while opening repository (" << path << ")" << endl;
		SetConsoleTextAttribute(hConsole, 12);
	}
	git_remote *remote = nullptr;
	int error = git_remote_lookup(&remote, repo, "origin");

	if (error >= 0)                     // has remote repository
	{
		if ((name.empty() == false) && (password.empty() == false)){
			git_fetch_options fetch_opts = GIT_FETCH_OPTIONS_INIT;
			fetch_opts.callbacks.credentials = cred_acquire_cb;             // setting credentials
			// fetch_opts.callbacks.credentials = git_cred_userpass;

			fetch_opts.update_fetchhead = 1;

			if (error = git_remote_fetch(remote, NULL, &fetch_opts, NULL) != 0) {
				const git_error *lastError = giterr_last();
				SetConsoleTextAttribute(hConsole, 12);
				cerr << "Problem with fetch, error message : '" << lastError->message << "'" << " repository : " << path << endl;
				SetConsoleTextAttribute(hConsole, 7);
			}

			remote_commits = numOfRemoteCommits(repo, path);
			local_commits = numOfLocalCommits(repo, path);

			result = getInfoIfActual(local_commits, remote_commits, path);
		}
		else {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "You have to set credentials !" << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
	}

	else  result = "Your repository is not remote = you have an actual version";

	git_remote_free(remote);
	git_repository_free(repo);
	CommitInfo commit_info;
	commit_info.message = result;
	commit_info.number = local_commits - remote_commits;
	return commit_info;
}

// create a file output.csv and prints the value of map <string, PackageInfo> package_info_map into it
void GitProjectInfo::outputToCsv(map <string, PackageInfo> package_info_map, string folder_path, string save_csv_there) {
	ofstream myfile;
	save_csv_there += "\\output.csv";

	myfile.open(save_csv_there.c_str(), ios::out);
	string relative_path;
	for (auto& kv : package_info_map) {
		/*string full_path = kv.second.local_path;
		relative_path = full_path.erase(0, folder_path.size());
		relative_path = relative_path.erase(relative_path.size() - 1, 1);*/
		myfile << kv.second.local_path << ";" << kv.second.url << ";" << kv.second.tag << ";";
		myfile << endl;
	}
	myfile.close();
}

// reading csv file and saving local paths from csv to vector<string> all_paths 
void GitProjectInfo::inputLocalPaths(string path) {
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	string line;

	ifstream my_file;
	my_file.open(path);
	if (my_file.is_open())
	{
		while (getline(my_file, line, ';'))
		{
			//if (line.find(":\\") != std::string::npos){
			paths.push_back(line);
			//}
		}
		my_file.close();
		/*for (unsigned int i = 0; i < all_paths.size(); i++){
		for (auto& kv : package_info_map) {
		if (kv.second.local_path == all_paths.at(i)) cout << kv.second.local_path << "\t\t" << kv.second.url << "\t" << kv.second.commit_info << endl;
		}
		}*/
	}
	else {
		SetConsoleTextAttribute(hConsole, 12);
		cerr << "Unable to open file";
		SetConsoleTextAttribute(hConsole, 7);
	}
}

// reading csv file and saving url adresses from csv to vector<string> all_urls
void GitProjectInfo::inputUrl(string path) {
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	string one_line, one_box;
	all_urls.clear();
	folder_name.clear();
	specific_tags.clear();
	ifstream my_file;
	my_file.open(path);

	if (my_file.is_open()) {
		int count = 0;
		while (getline(my_file, one_line, '\n')) {
			stringstream line;
			line.str(one_line);
			count = 0;
			while (getline(line, one_box, ';'))
			{
				count++;
				if ((count == 1) && (one_box.empty() == false)) {
					folder_name.push_back(one_box);
				}
				else if ((count == 2) && (one_box.empty() == false)) {
					size_t pos = one_box.find("http");
					if (pos != string::npos)
					{
						// all_urls.erase(std::remove(all_urls.begin(), all_urls.end(), one_box), all_urls.end());
						all_urls.push_back(one_box);

					}
				}
				else if ((count == 3) && (one_box.empty() == false)) {
					specific_tags.push_back(one_box);
				}
			}
		}
		my_file.close();
	}
	else {
		SetConsoleTextAttribute(hConsole, 12);
		cerr << "Unable to open file";
		SetConsoleTextAttribute(hConsole, 7);
	}
}

// fills folder_name-s, all_urls and their specific_tags
void GitProjectInfo::fillDataToClone(string name, string url, string tag) {
	folder_name.push_back(name);
	all_urls.push_back(url);
	specific_tags.push_back(tag);
}

// set name of folder in a given path
string GitProjectInfo::setNameOfFolder(int num_of_folder, string path) {
	/*
	// this sets your relative path to name of remote repo
	string name;
	istringstream iss(full_url);
	while (getline(iss, name, '/')){}             // after while loop variable 'name' contains name of local path with subfolder

	path += "\\";
	path += name;

	std::size_t found = path.find(".git");
	if (found != std::string::npos)	path.erase(found, 4);
	*/

	// (unsigned) because of warnings
	if (folder_name.size() >= (unsigned)num_of_folder){
		path += "\\";
		path += folder_name.at(num_of_folder);
		num_of_folder++;
	}
	return path;
}

// fill vector<string> clone_to , called by cloneRepository()
void GitProjectInfo::fillVectorCloneTo(string path) {
	clone_to.clear();
	for (unsigned int i = 0; i < all_urls.size(); i++){
		string tmp = setNameOfFolder(i, path);
		clone_to.push_back(tmp);
	}
}

// making clone of urls in vector<string> all_urls, filled by inputUrl()
void GitProjectInfo::doClone(git_clone_options clone_opts, int num) {
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

	git_repository *cloned_repo = NULL;
	// cout << all_urls.at(num).c_str() << " -> " << clone_to.at(num).c_str() << endl;

	int error = git_clone(&cloned_repo, all_urls.at(num).c_str(), clone_to.at(num).c_str(), &clone_opts);
	if (error != 0) {
		const git_error *err = giterr_last();
		SetConsoleTextAttribute(hConsole, 12);
		cerr << "error in clone num " << num << " -> message :" << err->message << endl;
		SetConsoleTextAttribute(hConsole, 7);
	}
	else cout << endl << "Clone " << num << " succesful" << "(from url : " << all_urls.at(num) << "    " << "to path : " << clone_to.at(num) << ")" << endl;
	git_repository_free(cloned_repo);
}

// calling doClone() for each url of vector<string> all_urls
void GitProjectInfo::cloneRepository(string path) {
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

	if ((name.empty() == false) && (password.empty() == false)){
		cout << endl << "Cloning remote repositories  to :" << path << endl << endl;
		vector<thread> threads_clone;

		if (all_urls.empty())  {							// uses function InputUrl() to get values to allUrls
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "Empty file with URLs" << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
		else {

			git_clone_options clone_opts = GIT_CLONE_OPTIONS_INIT;
			//git_checkout_options checkout_opts = GIT_CHECKOUT_OPTIONS_INIT;
			//clone_opts.checkout_opts = checkout_opts;

			clone_opts.fetch_opts.callbacks.credentials = cred_acquire_cb;
			//clone_opts.fetch_opts.callbacks.credentials = git_cred_userpass;

			fillVectorCloneTo(path);

			for (unsigned int i = 0; i < all_urls.size(); ++i)		threads_clone.push_back(thread(&GitProjectInfo::doClone, this, clone_opts, i));
			cout << endl << "Cloning... " << "(num of clones : " << all_urls.size() << " )" << endl;
			for (auto& th : threads_clone)							th.join();
			SetConsoleTextAttribute(hConsole, 10);
			cout << endl << "Clone successful" << endl;
			SetConsoleTextAttribute(hConsole, 7);
			// switching to specified tag
			for (unsigned int i = 0; i < specific_tags.size(); i++){
				if (specific_tags.size() >= clone_to.size()){
					string vers;
					if (specific_tags.at(i).find("master") != string::npos) vers = "refs/heads/" + specific_tags.at(i);
					else vers = "refs/tags/" + specific_tags.at(i);
					string pth = findLocalPath(clone_to.at(i));
					setToTag(pth, vers);
				}
			}
		}
	}
	else {
		SetConsoleTextAttribute(hConsole, 12);
		cerr << "You have to set credentials !" << endl;
		SetConsoleTextAttribute(hConsole, 7);
	}
}

// returns vector<string> all_paths
vector<string> GitProjectInfo::getAllPaths() {
	return this->all_paths;
}

// returns vector<string> all_urls
vector<string> GitProjectInfo::getAllUrls() {
	return this->all_urls;
}

// sets all urls
void GitProjectInfo::setAllUrls(vector<string> urls) {
	this->all_urls.clear();
	this->all_urls = urls;
}

// returns map<string,PackageInfo> package_info_map 
map<string, PackageInfo> GitProjectInfo::getPackageInfoMap() {
	return this->package_info_map;
}

int GitProjectInfo::getCounter() {
	return this->counter;
}

vector<string> temp_tags, temp_branches;

// method to get tags and save them to temp_tags
int each_ref(git_reference *ref, void *payload)
{
	string tag = git_reference_name(ref);

	if (tag.find("refs/tags") != std::string::npos) {
		temp_tags.push_back(tag);
	}
	else if (tag.find("refs/heads") != std::string::npos) {
		temp_branches.push_back(tag);
	}
	return 0;
}

// fill vector<string> tags with tags of given repository
void GitProjectInfo::loadTags(string path) {
	const char * REPO_PATH = path.c_str();
	git_repository * repo = nullptr;
	git_repository_open(&repo, REPO_PATH);

	git_reference_foreach(repo, each_ref, NULL);
	tags = temp_tags;
	branches = temp_branches;
	temp_tags.clear();
	temp_branches.clear();
	git_repository_free(repo);
}

// method to create lightweight tag from an actual version of code, path is path to given repo, tag_name is the name of the tag
void GitProjectInfo::createTag(string path, string tag_name) {
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	const char * REPO_PATH = path.c_str();
	git_repository * repo = nullptr;
	git_repository_open(&repo, REPO_PATH);

	git_reference * out;
	git_oid oid;
	tag_name = "refs/tags/" + tag_name;

	if (git_reference_name_to_id(&oid, repo, "HEAD") != 0) cerr << "" << endl;
	if (git_reference_create(&out, repo, tag_name.c_str(), &oid, true, NULL) != 0) {
		SetConsoleTextAttribute(hConsole, 12);
		cerr << "error creating reference : " << tag_name << endl;
		SetConsoleTextAttribute(hConsole, 7);
	}
	git_reference_free(out);
	git_repository_free(repo);

	// annotated tag 
	/*const char *local;
	local = git_repository_path(repo);
	string result = local;

	git_object *obj;
	string ref = gitDescribe(path);
	git_revparse_single(&obj, repo, ref.c_str()); // master

	git_signature *sig;
	git_signature_now(&sig, "Martin Majak", "Martin.MAJAK@frequentis.com");
	git_oid annotation_id;
	git_tag_create(
	&annotation_id,      // newly-created object's id
	repo,                // repository
	tag_name.c_str(), // tag name
	obj,                 // tag target
	sig,                 // signature
	"tag created by button",         // message
	1);                  // force if name collides

	git_signature_free(sig);
	git_object_free(obj);
	*/
}

vector<string> GitProjectInfo::getTags() {
	return tags;
}

vector<string> GitProjectInfo::getBranches() {
	return branches;
}

// method to find uncommitted changes in files 
int GitProjectInfo::gitDiff(string path, int mode) {
	int result;
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	const char * REPO_PATH = path.c_str();
	git_repository * repo = nullptr;
	git_repository_open(&repo, REPO_PATH);

	git_diff_options diffopts = GIT_DIFF_OPTIONS_INIT;
	git_diff *diff;
	diffopts.flags = GIT_CHECKOUT_NOTIFY_CONFLICT | GIT_CHECKOUT_NOTIFY_UNTRACKED;
	git_diff_index_to_workdir(&diff, repo, NULL, &diffopts);

	/*git_diff_format_t format = GIT_DIFF_FORMAT_NAME_ONLY;
	if (0 != git_diff_print(diff, format, print_cb , NULL)) cerr << "git_diff_print() failed" << endl;*/

	size_t num_deltas = git_diff_num_deltas(diff);
	if (num_deltas != 0){
		const git_diff_delta *delta = git_diff_get_delta(diff, 0);
		unsigned int i = 0;
		if (mode == 0){
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "Your local changes to the following files would be overwritten by checkout : " << endl;
			SetConsoleTextAttribute(hConsole, 7);
			while (i < num_deltas) {
				delta = git_diff_get_delta(diff, i);
				git_diff_file file = delta->new_file;
				cerr << "\t" << file.path << endl;
				i++;
			}
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "Please commit your changes before you switch branches or perform 'Force switch' to discard your changes " << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
		else if (mode == 3) {
			SetConsoleTextAttribute(hConsole, 12);
			cout << "Conflict files in " << path << "  : " << endl;
			SetConsoleTextAttribute(hConsole, 7);
			while (i < num_deltas) {
				delta = git_diff_get_delta(diff, i);
				git_diff_file file = delta->new_file;
				cout << file.path << endl;
				i++;
			}
			cout << endl;
		}
		result = 1;
	}
	else result = 0;

	git_diff_free(diff);
	git_repository_free(repo);
	return result;
}

// method to set program to specified tag, given as an input parameter
void GitProjectInfo::setToTag(string path, string tag) {
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	const char * REPO_PATH = path.c_str();
	git_repository * repo = nullptr;
	git_repository_open(&repo, REPO_PATH);

	git_reference *ref;
	git_reference_lookup(&ref, repo, "HEAD");

	git_reference *new_ref;
	git_reference_lookup(&new_ref, repo, tag.c_str());

	git_revwalk *walker;
	git_revwalk_new(&walker, repo);
	git_revwalk_push_ref(walker, tag.c_str());

	git_oid id;
	git_revwalk_next(&id, walker);

	git_reference_set_target(&new_ref, ref, &id, NULL);

	if (0 != git_repository_set_head_detached(repo, &id)) {
		SetConsoleTextAttribute(hConsole, 12);
		cerr << endl << "problem occured while detaching head, repository : " << path << " switching to tag " << tag << " (unknown tag for this repo)" << endl;
		SetConsoleTextAttribute(hConsole, 7);
	}

	git_checkout_options opts = GIT_CHECKOUT_OPTIONS_INIT;
	opts.checkout_strategy = GIT_CHECKOUT_FORCE;		// should be safe !!
	opts.notify_flags = GIT_CHECKOUT_NOTIFY_ALL;

	if (0 != git_checkout_head(repo, &opts)) {
		SetConsoleTextAttribute(hConsole, 12);
		cerr << "problem with git_checkout_head()" << endl;
		SetConsoleTextAttribute(hConsole, 7);
	}
	if (tag.find("refs/heads/") == string::npos) {
		string reference = tag + ".master";
		reference.erase(0, 10);
		git_reference * out;
		git_commit * commit;
		git_commit_lookup(&commit, repo, &id);
		git_branch_create(&out, repo, reference.c_str(), commit, 1);

		git_reference_set_target(&out, ref, &id, NULL);
		string full_ref = "refs/heads/" + reference;
		if (0 != git_repository_set_head(repo, full_ref.c_str())) {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << endl << "problem occured while setting head, repository : " << path << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
		git_checkout_notify_cb(opts.notify_flags);
		if (0 != git_checkout_head(repo, &opts)) {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "problem with git_checkout_head() " << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
	}
	else {
		string reference = tag;
		git_reference * out;
		git_reference_set_target(&out, ref, &id, NULL);
		if (0 != git_repository_set_head(repo, reference.c_str())) {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << endl << "problem occured while setting head, repository : " << path << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
		git_checkout_notify_cb(opts.notify_flags);
		if (0 != git_checkout_head(repo, &opts)) {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "problem with git_checkout_head() " << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
	}

	git_reference_free(ref);
	git_reference_free(new_ref);
	git_revwalk_free(walker);
	git_repository_free(repo);
}

// method to perform "git describe" 
string GitProjectInfo::gitDescribe(string path) {
	string result;
	const char * REPO_PATH = path.c_str();
	git_repository * repo = nullptr;
	git_repository_open(&repo, REPO_PATH);

	git_describe_result *out;
	git_describe_options opts = GIT_DESCRIBE_OPTIONS_INIT;
	opts.describe_strategy = GIT_DESCRIBE_ALL;
	opts.max_candidates_tags = 0;
	opts.only_follow_first_parent = 1;
	git_describe_workdir(&out, repo, &opts);
	git_buf out1 = { 0 };
	const git_describe_format_options opts1 = GIT_DESCRIBE_FORMAT_OPTIONS_INIT;
	git_describe_format(&out1, out, &opts1);
	result = out1.ptr;

	git_buf_free(&out1);
	git_describe_result_free(out);
	git_repository_free(repo);

	return result;
}

// performing "git add *" and calling createCommit() to create commit
int GitProjectInfo::addAll(string path, string commit_message){
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	int result;
	const char * REPO_PATH = path.c_str();
	git_repository * repo = nullptr;
	git_repository_open(&repo, REPO_PATH);

	// Get the git index.
	git_index * index = NULL;
	result = git_repository_index(&index, repo);
	SetConsoleTextAttribute(hConsole, 12);
	if (result != 0) cerr << giterr_last()->message << endl;

	// Add all files to the git index.
	result = git_index_add_all(index, NULL, 0, NULL, NULL);
	if (result != 0) cerr << giterr_last()->message << endl;

	// Write the index to disk.
	result = git_index_write(index);
	if (result != 0) cerr << giterr_last()->message << endl;
	SetConsoleTextAttribute(hConsole, 7);
	// Create commit
	result = createCommit(index, repo, commit_message);

	git_index_free(index);
	git_repository_free(repo);

	return result;
}

// create commit from index in repository with commit_message
int GitProjectInfo::createCommit(git_index * index, git_repository * repo, string commit_message) {
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	int result;
	git_signature *signature;
	git_commit *parent;
	git_tree *tree;
	git_oid tree_oid, parent_id, commit_oid;
	//time_t timev;

	/*string user_name = name;
	std::size_t found = user_name.find("@");
	if (found != std::string::npos) user_name.erase(found, user_name.size() - found);
	*/

	if (0 != git_index_write_tree(&tree_oid, index)) {
		SetConsoleTextAttribute(hConsole, 12);
		cerr << "error -> git_index_write_tree() not successful" << endl;
		SetConsoleTextAttribute(hConsole, 7);
	}
	/*if (name.empty() == false) git_signature_new(&signature, user_name.c_str(), name.c_str(), time(&timev), 60);
	else git_signature_new(&signature, "unknown_user", "credentials_not_set", time(&timev), 60);*/
	if (git_signature_default(&signature, repo) < 0)
		cerr << "Unable to create a commit signature.Perhaps 'user.name' and 'user.email' are not set" << endl;

	git_tree_lookup(&tree, repo, &tree_oid);
	git_reference_name_to_id(&parent_id, repo, "HEAD");
	git_commit_lookup(&parent, repo, &parent_id);

	if (commit_message.empty() == true) commit_message = "Commit message empty";
	result = git_commit_create_v(&commit_oid, repo, "HEAD", signature, signature, NULL, commit_message.c_str(), tree, 1, parent);
	if (result != 0) {
		SetConsoleTextAttribute(hConsole, 12);
		cerr << "error -> git_commit_create_v() failed" << endl;
		SetConsoleTextAttribute(hConsole, 7);
	}
	git_signature_free(signature);
	git_tree_free(tree);
	return result;
}

// return all tags in sent repository (in form refs/tags/tag:refs/tags/tag)
vector<string> GitProjectInfo::getAllRepoTags(git_repository *repo){
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	vector<string> repo_tags;
	git_strarray * tag_names = new git_strarray();
	if (git_tag_list(tag_names, repo) != 0) {
		SetConsoleTextAttribute(hConsole, 12);
		cerr << "error getting tag list " << endl;
		SetConsoleTextAttribute(hConsole, 7);
	}
	for (unsigned int i = 0; i < tag_names->count; i++)	{
		string tag_name = tag_names->strings[i];
		tag_name = "refs/tags/" + tag_name + ":refs/tags/" + tag_name;
		repo_tags.push_back(tag_name);
	}

	git_strarray_free(tag_names);
	return repo_tags;
}

void GitProjectInfo::performPushInThread(vector<string> local_paths) {
	vector<thread> threads;
	for (unsigned int i = 0; i < local_paths.size(); ++i)	threads.push_back(thread(&GitProjectInfo::push, this, local_paths.at(i)));
	for (auto& th : threads)				th.join();
}

// perform "git push" on sent repository
void GitProjectInfo::push(string path) {
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hConsole, 12);
	int result = 0;
	string branch_name;
	vector<string> references, tag_names;
	const char * REPO_PATH = path.c_str();
	git_repository * repo = nullptr;
	git_repository_open(&repo, REPO_PATH);

	if ((name.empty() == false) && (password.empty() == false)){
		// get all tags of repo
		tag_names = getAllRepoTags(repo);

		// get current branch name
		branch_name = getCurrentBranchRepo(repo);

		if (branch_name == "HEAD") branch_name = "master";
		string push_reference = "refs/heads/" + branch_name + ":refs/heads/" + branch_name;
		git_remote_callbacks callbacks = GIT_REMOTE_CALLBACKS_INIT;

		// get the remote.
		git_remote* remote = NULL;
		git_remote_lookup(&remote, repo, "origin");

		// clear push references before push
		if (git_remote_refspec_count(remote) != 1) clearPushRefs(path);

		// add new push references
		if (0 != git_remote_add_push(repo, git_remote_name(remote), push_reference.c_str())) cerr << "git_remote_add_push() returned error value!" << endl;

		// add push ref to all tags in repository
		for (unsigned int i = 0; i < tag_names.size(); i++) {
			if (0 != git_remote_add_push(repo, git_remote_name(remote), tag_names.at(i).c_str())) cerr << "git_remote_add_push() returned error value!" << endl;
		}

		// push refs are added after second lookup
		git_remote_lookup(&remote, repo, "origin");

		// connect to remote
		callbacks.credentials = cred_acquire_cb;
		//callbacks.credentials = git_cred_userpass;

		git_remote_connect(remote, GIT_DIRECTION_PUSH, &callbacks, NULL, NULL);
		if (1 != git_remote_connected(remote)) cerr << "problem connecting to remote" << endl;

		// configure options
		git_push_options options;
		if (0 != git_push_init_options(&options, GIT_PUSH_OPTIONS_VERSION)) cerr << "git_push_init_options() returned error value!" << endl;

		// do the push
		result = git_remote_upload(remote, NULL, &options);
		if (result != 0) cerr << "git_remote_upload() failed" << endl;
		git_remote_disconnect(remote);
		git_remote_free(remote);
	}
	else cerr << "You have to set credentials !" << endl;
	SetConsoleTextAttribute(hConsole, 7);
	git_repository_free(repo);

	if (result != 0) {
		SetConsoleTextAttribute(hConsole, 12);
		cerr << "Problem with push, repository : " << path << endl;
		SetConsoleTextAttribute(hConsole, 7);
	}
	else {
		SetConsoleTextAttribute(hConsole, 10);
		cout << "Push successful, repository : " << path << endl;
		SetConsoleTextAttribute(hConsole, 7);
	}
}

// get currently switched branch of sent repository
string GitProjectInfo::getCurrentBranchRepo(git_repository *repo) {
	string branch_name;
	git_reference *head = NULL;
	int error = git_repository_head(&head, repo);
	if (!error) {
		branch_name = git_reference_shorthand(head);
	}
	git_reference_free(head);

	return branch_name;
}

// return name of current branch in sent repository
string GitProjectInfo::getCurrentBranch(string path) {
	string branch_name;
	const char * REPO_PATH = path.c_str();
	git_repository * repo = nullptr;
	git_repository_open(&repo, REPO_PATH);

	git_reference *head = NULL;
	int error = git_repository_head(&head, repo);
	if (!error) {
		branch_name = git_reference_shorthand(head);
	}

	git_reference_free(head);
	git_repository_free(repo);
	return branch_name;
}

// edit config file (delete all push refs)
void GitProjectInfo::clearPushRefs(string path) {
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	path += "/.git/config"; // config file
	string line;
	vector<string> lines_from_file;
	ifstream my_file;
	std::ofstream ofs;
	ofstream myfile;

	// read file and save all lines besides push refs
	my_file.open(path);
	int counter = 0;
	int can_from = 1000;
	if (my_file.is_open())
	{
		while (getline(my_file, line))
		{
			if (line.find("[remote \"origin\"]") != std::string::npos) can_from = counter + 3;
			if ((can_from <= counter) && (line.find("push") != std::string::npos) && (line.find("http") == std::string::npos)) {}		// this line wouldn't be in the new config file
			else if (line.find("push = ") == std::string::npos) lines_from_file.push_back(line);
			counter++;
		}
		my_file.close();
	}
	else {
		SetConsoleTextAttribute(hConsole, 12);
		cerr << "Unable to open file";
		SetConsoleTextAttribute(hConsole, 7);
	}
	// clear config file
	ofs.open(path, std::ofstream::out | std::ofstream::trunc);
	ofs.close();

	// fill up everything apart from push refs
	myfile.open(path, ios::out);
	for (unsigned int i = 0; i < lines_from_file.size(); i++) myfile << lines_from_file.at(i) << endl;
	myfile.close();
}

// create initial commit after initializing empty repository -> this initial commit doesn't record any changes!
static int create_initial_commit(git_repository *repo)
{
	git_signature *sig;
	git_index *index;
	git_oid tree_id, commit_id;
	git_tree *tree;

	if (git_signature_default(&sig, repo) < 0)
		cerr << "Unable to create a commit signature.Perhaps 'user.name' and 'user.email' are not set" << endl;
	if (git_repository_index(&index, repo) < 0)
		cerr << "Could not open repository index" << endl;

	if (git_index_write_tree(&tree_id, index) < 0)
		cerr << "Unable to write initial tree from index" << endl;

	git_index_free(index);
	if (git_tree_lookup(&tree, repo, &tree_id) < 0)
		cerr << "Could not look up initial tree" << endl;
	if (git_commit_create_v(&commit_id, repo, "HEAD", sig, sig, NULL, "Initial commit", tree, 0) < 0) {
		cerr << "Could not create the initial commit" << endl;

		git_tree_free(tree);
		git_signature_free(sig);
		return 1;
	}
	else {
		git_tree_free(tree);
		git_signature_free(sig);
		return 0;
	}
}

// initialize empty git repository in sent local path
int GitProjectInfo::initializeRepo(string path) {
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	git_repository *repo = nullptr;
	int result = 0;

	if (git_repository_open(&repo, path.c_str()) != 0){
		SetConsoleTextAttribute(hConsole, 12);
		if (git_repository_init(&repo, path.c_str(), false) != 0) cerr << "error creating repository : repo(" << path << ")" << endl;
		if (git_repository_open(&repo, path.c_str()) != 0) cerr << "error opening repository : repo(" << path << ")" << endl;
		result = create_initial_commit(repo);
		SetConsoleTextAttribute(hConsole, 7);
		git_repository_free(repo);
		return result;
	}
	else {
		cerr << "Given path ( " << path << " ) is already a git repo!" << endl;
		return 1;
	}
}

// returns all subfolders of sent folder
vector<string> GitProjectInfo::findSubfold(string path) {
	vector<string> result;
	DIR *dir;
	struct dirent *ent;
	const char* loc = path.c_str();

	if ((dir = opendir(loc)) != NULL) {
		while ((ent = readdir(dir)) != NULL) {                      // searching through all folders
			string subfold = ent->d_name;
			if ((subfold != ".") && (subfold != "..")){
				string add_path = path;
				add_path += "\\";
				add_path += subfold;
				result.push_back(add_path);
			}
		}
		closedir(dir);
	}
	return result;
}

// add remote from sent url to sent repository
string GitProjectInfo::addRemoteOrigin(string path, string remote_url) {
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

	if (name.empty() == false && password.empty() == false){
		git_repository *repo = nullptr;
		string url = remote_url;
		if (git_repository_open(&repo, path.c_str()) != 0) {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "Error opening repository : repo(" << path << ")" << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
		git_strarray * out = new git_strarray();
		if (git_remote_list(out, repo) != 0) {
			SetConsoleTextAttribute(hConsole, 12);
			cerr << "Error creating remote list" << endl;
			SetConsoleTextAttribute(hConsole, 7);
		}
		if (out->count != 0) {
			cerr << "Given path ( " << path << " ) already has a remote !" << endl;
			url = "already_has_remote";
		}
		else {
			string folder_name;
			git_remote *remote;

			path.erase(path.size() - 1, 1);
			istringstream iss(path);
			while (getline(iss, folder_name, '/')){}

			url += "/";
			url += folder_name;
			url += ".git";

			// creating remote repository
			string command = "\"curl --user " + name + ":" + password + " https://api.bitbucket.org/1.0/repositories/ " + "--data name=" + folder_name + " --data is_private='true'\"";
			string full_command = "\"\"C:\\Program Files\\Git\\bin\\sh.exe\" --login -i -c " + command + "\"";
			system(full_command.c_str());

			// connect to remote, create origin
			if (git_remote_create(&remote, repo, "origin", url.c_str()) != 0) {
				SetConsoleTextAttribute(hConsole, 12);
				cerr << "error creating remote : repo(" << path << ")" << endl;
				SetConsoleTextAttribute(hConsole, 7);
			}
			git_remote_free(remote);
		}

		git_repository_free(repo);
		return url;
	}
	else {
		SetConsoleTextAttribute(hConsole, 12);
		cerr << "You have to set credentials " << endl;
		SetConsoleTextAttribute(hConsole, 7);
		return "";
	}
}

// return information about credentials -> if they are set - returns 0, else returns 1
int GitProjectInfo::areCredentialsSet() {
	if ((user_name.empty() == false) && (user_pass.empty() == false)) return 1;
	else return 0;
}

// show conflict files between csv file and selected folder
void GitProjectInfo::showCsvConflicts(multimap<string, PackageInfo> to_switch, vector<string> conflict_tags) {
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	vector<string> specific_tag;
	string latest_tag, item, numb1, numb2;

	for (unsigned int j = 0; j < conflict_tags.size(); j++){
		// load tags from csv files
		for (auto& walker : to_switch){
			if (walker.second.local_path.find(conflict_tags.at(j)) != string::npos)	specific_tag.push_back(walker.second.tag);
		}

		// if you have less then 2 tags -> break
		if (specific_tag.size() < 2) break;
		latest_tag = specific_tag.at(0);
		for (unsigned int i = 1; i < specific_tag.size(); i++){
			string tag__ = latest_tag;
			size_t found = tag__.find(".");
			tag__.erase(remove(tag__.begin() + found, tag__.end(), '.'), tag__.end());
			double latest_tag_num = stod(tag__);

			// find the biggest
			size_t n = count(specific_tag.at(i).begin(), specific_tag.at(i).end(), '.');
			string str = specific_tag.at(i);

			if (n > 1) {
				found = str.find(".");
				str.erase(remove(str.begin() + found, str.end(), '.'), str.end());
			}

			double tag_number = stod(str);
			if (tag_number > latest_tag_num) latest_tag = specific_tag.at(i);
		}

		// write the biggest
		for (auto& walker : to_switch){
			if (walker.second.local_path.find(conflict_tags.at(j)) != string::npos) walker.second.tag = latest_tag;
		}

		// clear
		latest_tag.clear();
		specific_tag.clear();
	}
}