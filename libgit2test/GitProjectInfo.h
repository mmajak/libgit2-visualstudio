//
// Created by mmajak on 20.2.2017.
//

#ifndef LIBGIT2TEST_GITPROJECTINFO_H
#define LIBGIT2TEST_GITPROJECTINFO_H
#include <iostream>
#include <map>
#include <vector>
#include <C:/Users/mmajak/Documents/Visual studio 2013/Projects/libgit2-0.25.1/include/git2.h>
using namespace std;

struct PackageInfo{
	string local_path;
	string url;
	string commit_info;
	int commit_number;
	string tag;
};

struct CommitInfo{
	string message;
	int number;
};

class GitProjectInfo {

public:
	GitProjectInfo();
	~GitProjectInfo();
	void getPackages(string path);
	void print();
	void outputToCsv(map <string, PackageInfo> package_info_map, string cut_full_path, string save_csv_there);
	void inputLocalPaths(string path);
	void inputUrl(string path);
	void cloneRepository(string path);
	string findUrl(string path);
	string findLocalPath(string path);
	void GitProjectInfo::fillAllFetchData(vector<string> to_fetch_paths);
	vector<string> getAllPaths();
	void setAllUrls(vector<string> urls);
	vector<string> getAllUrls();
	map<string, PackageInfo> getPackageInfoMap();
	int getCounter();
	void createTag(string path, string tag_name);
	void setToTag(string path, string tag);
	void loadTags(string path);
	vector<string> getTags();
	int addAll(string path,string commit_message);
	int createCommit(git_index * index, git_repository * repo, string commit_message);
	string gitDescribe(string path); 
	int gitDiff(string path,int mode);
	string getCurrentBranch(string path);
	vector<string> getBranches();
	void setCredentials();
	int initializeRepo(string path);
	vector<string> findSubfold(string path);
	string addRemoteOrigin(string path, string remote_url);
	int areCredentialsSet();
	void performPushInThread(vector<string> local_paths);
	void fillDataToClone(string name,string url,string tag);
	void showCsvConflicts(multimap<string, PackageInfo> to_switch, vector<string> conflict_tags);

private:
	vector<string> all_paths;																	// all the local paths to the .git repositories
	map <string, PackageInfo> package_info_map;													// full information about all .git repositories
	map <string, CommitInfo> commit_info_map;													// information about commit -> message and num of commits ahead/behind
	vector<string> all_urls;																	// all URL adresses, loaded from a file input1 by inputUrl()
	vector<string> clone_to;																	// all paths loaded by fillVectorCloneTo(), cloned repositories will be in these paths
	int counter = 0;																			// number of .git repositories in given path
	vector<string> tags;																		// all the tags in one repository
	vector<string> branches;
	vector<string> paths;
	vector<string> specific_tags;																// specific current tags of repositories (from csv)
	vector<string> folder_name;																	// specific folder names from csv file
	string user_name, user_pass;
	
	void fillAllData(PackageInfo* package_info);
	void fillData(PackageInfo* package_info, int index);
	void GitProjectInfo::fillFetchData(CommitInfo* info_fetch, int index, string path);
	void findIfIncludeGit(string name, string loc, PackageInfo* package_info);
	void findSubfolders(string loc, PackageInfo* package_info);
	void findNumOfRepo(string loc);
	int numOfRemoteCommits(git_repository *repo,string path);
	int numOfLocalCommits(git_repository *repo,string path);
	string getInfoIfActual(int local_commits, int remote_commits,string path);
	CommitInfo checkIfActual(string path);
	string setNameOfFolder(int num_of_folder, string path);
	void fillVectorCloneTo(string path);
	void doClone(git_clone_options clone_opts, int num);
	string getCurrentBranchRepo(git_repository *repo);
	void clearPushRefs(string path);
	vector<string> getAllRepoTags(git_repository *repo);
	void push(string path);

};

#endif //LIBGIT2TEST_GITPROJECTINFO_H